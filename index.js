/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	// Greeting

		function printHelloToUser (){
			let name = prompt("What is your name?");

			console.log ("Hello, " + name);
		}

		printHelloToUser ();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	// Get Age

		function getAge (){
			let age = prompt("How old are you?");

			console.log ("You are " + age + " years old.")
		}

		getAge ();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	// Get Address

		function getAddress (){
			let address = prompt("Where do you live?");

			console.log ("You live in " + address);
		}

		getAddress ();

		

function showUserAlert(){
		alert("Thank you for your inputs!");

	}

	 showUserAlert();  

 //Favorite Artists 
function favoriteMusicArtist(){
	let artistOne = ("1. David Archuleta");
	let artistTwo = ("2. Dept.");
	let artistThree = ("3. IU");
	let artistFour = ("4. Kai Kaitan");
	let artistFive = ("5. Keshi");


	console.log(artistOne);
	console.log(artistTwo);
	console.log(artistThree);
	console.log(artistFour);
	console.log(artistFive);
}

favoriteMusicArtist();

function myFiveFavoriteMovies (){
	let movieOne = ("The Godfather");
	let ratingOne = ("Rotten Tomatoes Rating: 97%");
	let movieTwo = ("The Godfather, Part II");
	let ratingTwo = ("Rotten Tomatoes Rating: 96%");
	let movieThree = ("Shawshank Redemption");
	let ratingThree = ("Rotten Tomatoes Rating: 91%");
	let movieFour = ("To Kill a Mockingbird");
	let ratingFour = ("Rotten Tomatoes Rating: 93%");
	let movieFive = ("Pyscho");
	let ratingFive = ("Rotten Tomatoes Rating: 96%");

	console.log(movieOne);
	console.log(ratingOne);
	console.log(movieTwo);
	console.log(ratingTwo);
	console.log(movieThree);
	console.log(ratingThree);
	console.log(movieFour);
	console.log(ratingFour);
	console.log(movieFive);
	console.log(ratingFive);
}

myFiveFavoriteMovies ();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
